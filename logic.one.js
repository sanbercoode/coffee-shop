let _ = require('lodash');
const rawData = [
  {
    id: 1,
    backgroundColor: 'blue',
    category: 'ayam',
  },
  {
    id: 2,
    backgroundColor: 'blue',
    category: 'ayam',
  },
  {
    id: 2.5,
    backgroundColor: 'baruiniiiii',
    category: 'burung',
  },
  {
    id: 3,
    backgroundColor: 'blue',
    category: 'ayam',
  },
  {
    id: 4,
    backgroundColor: 'yellow',
    category: 'burung',
  },
  {
    id: 5,
    backgroundColor: 'yellow',
    category: 'burung',
  },
  {
    id: 6,
    backgroundColor: 'yellow',
    category: 'burung',
  },
  {
    id: 7,
    backgroundColor: 'yellow',
    category: 'cabe',
  },
  {
    id: 8,
    backgroundColor: 'five',
    category: 'cabe',
  },
  {
    id: 9,
    backgroundColor: 'five',
    category: 'cabe',
  },
  {
    id: 10,
    backgroundColor: 'five',
    category: 'cabe',
  },
];

rawData.sort((a, b) => (a.category > b.category ? 1 : -1));

function myProcess() {
  // this lodash function will return backgroundColor value in string
  // {
  //   backgroundColor: 'yellow, five, five, five' ,
  //   category: 'cabe'
  // }
  let merged = _.uniqWith(rawData, (pre, cur) => {
    if (pre.category == cur.category) {
      cur.backgroundColor = cur.backgroundColor + ',' + pre.backgroundColor;
      return true;
    }
    return false;
  });
  // this method will convert backgroundColor string into array of string
  merged.forEach(element => {
    element.backgroundColor = element.backgroundColor.split(',');
  });
  return merged;
}
console.log(myProcess());
