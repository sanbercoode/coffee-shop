import 'react-native-gesture-handler'

import * as React from 'react'
import {NavigationContainer} from '@react-navigation/native'

import Detail from './screen/Detail/Detail';
import Home from './screen/Home/HomeScreen';
import Login from './screen/Login/LoginScreen';
import Register from './screen/Login/RegisterScreen';
import StackDetail from './navigation/index';

function App() {
  return (
      <NavigationContainer>
        {/* <Register /> */}
        {/* <Login /> */}
        {/* <Home /> */}
        {/* <Detail /> */}
        <StackDetail />
    </NavigationContainer>
  );
}

export default App;
