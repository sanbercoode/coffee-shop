import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

function Button({theTitle}) {
  return (
    <TouchableOpacity style={[styles.buttonComponent]}>
      <View>
        <Text style={[styles.text, {color: '#ffffff'}]}>{theTitle}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  buttonComponent: {
    borderRadius: 20,
    height: height * 0.05,
    width: width * 0.45,
    backgroundColor: '#3F2710',
    marginTop: height * 0.02,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontFamily: 'Fredoka-VariableFont_wdth,wght',
    fontSize: height * 0.023,
    color: '#3F2710',
    fontWeight: '700',
  },
});
export default Button;
