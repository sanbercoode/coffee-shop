import React, {useState} from 'react';
import {
  View,
  Dimensions,
  Image,
  TouchableOpacity,
  StyleSheet,
  TextInput,
} from 'react-native';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

export function ShowHide() {
  const [shouldShow, setShouldShow] = useState(false);

  function Show() {
    return (
      <View
        style={{
          height: height * 0.06,
          width: width,
          backgroundColor: 'rgba(217,217,217,0.5)',
          alignSelf: 'center',
          marginTop: height * 0.02,
          flexDirection: 'row',
        }}>
        <View
          style={{
            flex: 0.1,
          }}></View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            flex: 1,
          }}>
          <TextInput
            style={styles.text}
            placeholder="Search . . ."
            autoFocus={true}
            onEndEditing={() => setShouldShow(false)}
          />
        </View>
        <View style={{flex: 0.1}}></View>
      </View>
    );
  }

  function Hide() {
    return (
      <View
        style={{
          height: height * 0.06,
          width: width * 0.85,
          alignSelf: 'center',
          marginTop: height * 0.02,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <TouchableOpacity>
          <Image source={require('../assets/deddy.png')} />
        </TouchableOpacity>
        <View>
          <TouchableOpacity onPress={() => setShouldShow(true)}>
            <Image source={require('../assets/search.png')} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  return shouldShow ? <Show /> : <Hide />;
}

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Fredoka-VariableFont_wdth,wght',
    fontSize: height * 0.022,
    fontWeight: '600',
    paddingLeft: width * 0.015,
    paddingTop: width * 0.015,
  },
});
