import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
} from 'react-native';
import {ShowHide} from '../../components/SearchBar';
import data from './DUMMY';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

function Home({navigation}) {
  function Box({item}) {
    const [like, isLike] = useState(item.isLike);
    return (
      <View style={styles.box}>
        <View
          style={{
            flex: 0.15,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text style={[styles.text, {fontSize: height * 0.022}]}>
            ${item.price}
          </Text>
          <View
            style={{
              justifyContent: 'center',
              marginRight: width * 0.015,
            }}>
            <TouchableOpacity onPress={() => isLike(!like)}>
              <Image
                source={
                  like
                    ? require('../../assets/likesred.png')
                    : require('../../assets/likes.png')
                }
              />
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity
          style={{flex: 1}}
          onPress={() => {
            navigation.navigate('Detail');
          }}>
          <View style={{flex: 1}}>
            <View
              style={{
                flex: 1.5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image source={item.source} />
            </View>
            <View style={{flex: 1}}>
              <Text style={[styles.text, {fontSize: height * 0.022}]}>
                {item.name}
              </Text>
              <Text
                style={[
                  styles.text,
                  {
                    fontSize: height * 0.018,
                    color: '#BBA74B',
                  },
                ]}>
                {item.description}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
  return (
    <View>
      <ImageBackground
        style={styles.background}
        source={require('../../assets/homeBackground.png')}
      />
      <ShowHide />
      <View style={styles.content}>
        <ScrollView>
          {data.map(element => {
            return (
              <View key={element.id}>
                <Text style={styles.text}>{element.title}</Text>
                <View style={{flexDirection: 'row'}}>
                  <FlatList
                    keyExtractor={item => item.id}
                    data={element.data}
                    horizontal
                    renderItem={item => <Box item={item.item} />}
                  />
                </View>
              </View>
            );
          })}
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  box: {
    height: height * 0.3,
    width: width * 0.4,
    backgroundColor: '#3F2710',
    borderRadius: 15,
    marginRight: width * 0.02,
    marginLeft: width * 0.015,
  },
  background: {
    height: height,
    width: width,
    position: 'absolute',
  },
  content: {
    height: height * 0.74,
    width: width * 0.85,
    marginTop: height * 0.14,
    alignSelf: 'center',
  },
  text: {
    color: '#ffffff',
    fontFamily: 'Fredoka-VariableFont_wdth,wght',
    fontSize: height * 0.028,
    fontWeight: '600',
    paddingLeft: width * 0.015,
    paddingTop: width * 0.015,
  },
});

export default Home;
