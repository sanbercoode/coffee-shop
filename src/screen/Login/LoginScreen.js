import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  ImageBackground,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Button from '../../components/Button';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

function Login() {
  return (
    <View style={styles.container}>
      <ImageBackground
        source={require('../../assets/background.png')}
        style={styles.background}
      />
      <Text style={styles.title}>PICUTM COFFEE</Text>
      <Image
        style={styles.cangkir}
        source={require('../../assets/cangkir.png')}
      />
      <View style={styles.content}>
        <Text style={[styles.text, {alignSelf: 'flex-start'}]}>Email</Text>
        <TextInput style={styles.textInput} placeholder="youremail@email.com" />
        <Text
          style={[
            styles.text,
            {paddingTop: height * 0.01, alignSelf: 'flex-start'},
          ]}>
          Password
        </Text>

        <TextInput style={styles.textInput} placeholder="yourpassword" />
        <Button theTitle={'Log In'} />
        <TouchableOpacity>
          <Text
            style={[
              styles.text,
              {paddingTop: height * 0.01, fontSize: height * 0.018},
            ]}>
            Don't have account ? Register here
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {alignItems: 'center'},
  background: {
    height: height,
    width: width,
    position: 'absolute',
  },
  title: {
    fontFamily: 'FreckleFace-Regular',
    paddingTop: height * 0.12,
    fontSize: height * 0.04,
    color: '#3F2710',
  },
  text: {
    fontFamily: 'Fredoka-VariableFont_wdth,wght',
    fontSize: height * 0.023,
    color: '#3F2710',
    fontWeight: '700',
  },
  cangkir: {
    marginTop: height * 0.01,
    marginLeft: width * 0.08,
    height: height * 0.36,
    width: width * 0.62,
  },
  content: {
    height: height * 0.4,
    width: width * 0.7,
    marginTop: height * 0.018,
    alignItems: 'center',
  },
  textInput: {
    height: height * 0.05,
    width: width * 0.7,
    backgroundColor: '#ffffff',
    borderRadius: 12,
    marginTop: height * 0.01,
    borderWidth: 1,
  },
});
export default Login;
