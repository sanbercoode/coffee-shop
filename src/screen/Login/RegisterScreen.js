import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  ImageBackground,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Button from '../../components/Button';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const listComp = [
  {title: 'First Name', placeholder: 'Your First Name'},
  {title: 'Last Name', placeholder: 'Your Last Name'},
  {title: 'Email', placeholder: 'Your Email'},
  {title: 'Password', placeholder: 'Your Password'},
  {title: 'Re-type Password', placeholder: 'Re-type Password'},
];
function Register() {
  return (
    <View style={styles.container}>
      <ImageBackground
        source={require('../../assets/TransparentCup.png')}
        style={styles.background}
      />
      <Text style={styles.title}>PICUTM COFFEE</Text>

      <View style={styles.content}>
        {listComp.map(e => {
          return (
            <View>
              <Text
                style={[
                  styles.text,
                  {paddingTop: height * 0.01, alignSelf: 'flex-start'},
                ]}>
                {e.title}
              </Text>
              <TextInput style={styles.textInput} placeholder={e.placeholder} />
            </View>
          );
        })}
        <Button theTitle={'Register'} />
        <TouchableOpacity>
          <Text
            style={[
              styles.text,
              {paddingTop: height * 0.01, fontSize: height * 0.018},
            ]}>
            Log In with different account
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {alignItems: 'center'},
  background: {
    height: height,
    width: width,
    position: 'absolute',
  },
  title: {
    fontFamily: 'FreckleFace-Regular',
    paddingTop: height * 0.12,
    fontSize: height * 0.04,
    color: '#3F2710',
  },
  text: {
    fontFamily: 'Fredoka-VariableFont_wdth,wght',
    fontSize: height * 0.023,
    color: '#3F2710',
    fontWeight: '700',
  },
  content: {
    height: height * 0.4,
    width: width * 0.7,
    marginTop: height * 0.018,
    alignItems: 'center',
  },
  textInput: {
    height: height * 0.05,
    width: width * 0.7,
    backgroundColor: '#ffffff',
    borderRadius: 12,
    marginTop: height * 0.01,
    borderWidth: 1,
  },
});
export default Register;
