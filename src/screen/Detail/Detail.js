import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const lorem = `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
`;

function Detail({navigation}) {
  const [defaultRating, setDefaultRating] = useState(2);
  const [maxRating, setMaxRating] = useState([1, 2, 3, 4]);

  const whiteStar = require('../../assets/whiteStar.png');
  const yellowStar = require('../../assets/yellowStar.png');

  const CustomRating = () => {
    return (
      <View
        style={{
          flexDirection: 'row',
        }}>
        {maxRating.map((item, key) => {
          return (
            <TouchableOpacity
              activeOpacity={0.7}
              key={item}
              onPress={() => setDefaultRating(item)}>
              <Image
                source={item <= defaultRating ? yellowStar : whiteStar}
                style={{margin: width * 0.005}}
              />
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };
  return (
    <View>
      <View
        style={{
          height: height,
          justifyContent: 'flex-end',
        }}>
        <View
          style={{
            alignItems: 'center',
          }}>
          <Image
            source={require('../../assets/coffeeBig.png')}
            style={{height: height * 0.4, width: '100%', top: height * 0.05}}
          />
        </View>
        <View
          style={{
            backgroundColor: '#3F2710',
            borderTopLeftRadius: 30,
            borderTopRightRadius: 30,
            height: '70%',
            position: 'relative',
          }}>
          <View
            style={{
              alignSelf: 'center',
              width: width * 0.85,
              marginTop: height * 0.1,
              //   backgroundColor: 'pink',
            }}>
            <Text style={[styles.text, {fontWeight: 'bold'}]}>
              Machiato Hot
            </Text>
            <View style={{flexDirection: 'row', marginTop: height * 0.01}}>
              <View
                style={{
                  // backgroundColor: 'black',
                  flex: 0.7,
                }}>
                <View
                  style={{
                    backgroundColor: '#BBA74B',
                    height: height * 0.03,
                    width: width * 0.2,
                    borderRadius: 30,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={[
                      styles.text,
                      {
                        fontSize: height * 0.015,
                        paddingTop: 0,
                        fontWeight: 'bold',
                      },
                    ]}>
                    Espresso
                  </Text>
                </View>
              </View>
              <View
                style={{
                  //   backgroundColor: 'red',
                  flex: 0.8,
                  justifyContent: 'center',
                }}>
                <CustomRating />
              </View>
              <View
                style={{
                  //   backgroundColor: 'green',
                  flex: 1,
                  justifyContent: 'center',
                }}>
                <Text
                  style={[
                    styles.text,
                    {fontSize: height * 0.015, fontWeight: 'bold'},
                  ]}>
                  250 people like this
                </Text>
              </View>
            </View>
            <Text
              style={[
                styles.text,
                {paddingTop: height * 0.03, fontWeight: 'bold'},
              ]}>
              About
            </Text>
            <View
              style={{
                flexDirection: 'row',
                height: height * 0.3,
                justifyContent: 'space-between',
              }}>
              <View
                style={{
                  width: width * 0.65,
                  //   backgroundColor: 'grey',
                }}>
                <ScrollView>
                  <View>
                    <Text
                      style={[
                        styles.text,
                        {fontSize: height * 0.02, textAlign: 'justify'},
                      ]}>
                      {lorem}
                    </Text>
                  </View>
                </ScrollView>
              </View>
              <View
                style={{
                  backgroundColor: '#BBA74B',
                  height: height * 0.23,
                  width: 50,
                  borderRadius: 30,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <TouchableOpacity>
                  <View style={[styles.upDown, {backgroundColor: '#3F2710'}]}>
                    <Text style={[styles.text, {fontWeight: 'bold'}]}>+</Text>
                  </View>
                </TouchableOpacity>
                <Text
                  style={[
                    styles.text,
                    {fontSize: height * 0.03, fontWeight: 'bold'},
                  ]}>
                  01
                </Text>
                <TouchableOpacity>
                  <View style={[styles.upDown, {backgroundColor: '#ffffff'}]}>
                    <Text
                      style={[
                        styles.text,
                        {color: '#3F2710', fontWeight: 'bold'},
                      ]}>
                      -
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
              //   height: height * 0.05,
              width: width * 0.85,
              justifyContent: 'space-between',
              alignSelf: 'center',
              //   backgroundColor: 'blue',
              marginTop: height * 0.02,
            }}>
            <View style={{width: width * 0.65}}>
              <TouchableOpacity>
                <View
                  style={{
                    backgroundColor: '#BBA74B',
                    height: height * 0.07,
                    borderRadius: 30,
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={[
                      styles.text,
                      {
                        fontWeight: 'bold',
                        alignSelf: 'center',
                      },
                    ]}>
                    Order Now
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                // backgroundColor: 'red',
              }}>
              <Text
                style={[
                  styles.text,
                  {fontWeight: 'bold', fontSize: height * 0.04},
                ]}>
                $30
              </Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  box: {
    height: height * 0.3,
    width: width * 0.4,
    backgroundColor: '#3F2710',
    borderRadius: 15,
    marginRight: width * 0.02,
  },
  text: {
    color: '#ffffff',
    fontFamily: 'Fredoka-VariableFont_wdth,wght',
    fontSize: height * 0.028,
    fontWeight: '600',
  },
  upDown: {
    height: height * 0.05,
    width: height * 0.05,
    borderRadius: 90,
    margin: height * 0.01,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Detail;
