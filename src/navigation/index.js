import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../screen/Home/HomeScreen';
import Detail from '../screen/Detail/Detail';

const Stack = createStackNavigator();

function StackDetail() {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Detail" component={Detail} />
    </Stack.Navigator>
  );
}

export default StackDetail;
